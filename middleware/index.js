/*
* this page will accessed when you only declaired the page in other page.
* make it easy when you want to destructuring function in ES 6
* 
* for example, when you want to access validator only you can declaire :
* const {validator} = require ('./middleware')
* 
* or you can access whole middleware and call it like an object. example :
* const middleware = require ('./middleware')
* middleware.check()
* to check the token
*/
module.exports = {
	notFound : require('./notFound'), // when page not found
	check : require('./check'), // checking the token 
	login : require('./login'), // to create the token and store it at ./data/token.json
	logout : require('./logout'), // delete token from ./data/token.json
	validator : require('./validator'), // validating input from form
}