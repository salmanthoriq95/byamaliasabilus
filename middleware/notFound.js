/**
 * middleware when the page is not found
 */

module.exports = function (req,res) {
    res.status(404);
    return res.send({
        status :false,
        message : 'page not found',
        method : req.method,
        url : req.url
    });
}

