/**
 * validating all input from form.
 * if the input not validate then it will return error message
 * if the input validate then it will next()
 * 
 * I divide it become 2 kind of validator, that Create and Update.
 * if you looking for some specific validator just search the path and add Create or Update behind it
 */

//import joi to validate
const joi = require('joi')

/**
 * function to validate the input with schema inside.
 * 
 * @param {object} schema object that contain schema validation of the input
 * @param {*} req req object from express
 * @param {*} res res object from express
 * @param {*} next funtion to execute the next function
 * @returns 
 */
const errorMessage = (schema, req, res, next) => {
    const {error} = schema.validate(req.body); //validate the input with schema
    if (error) { // if not validate
        res.status (400);
        return res.send({
            status : false,
            message : error.message,
            data : null,
            method : req.method,
            url : req.url
        })
    }
    return next();
}

//start function
module.exports = {
    //middleware for create admin
    adminCreate : (req, res, next) => {
        const schema = joi.object({
            username : joi.string().alphanum().min(5).max(30).required(), //must contain 5-30 alphanumeric
            pass : joi.string().pattern(new RegExp(/^[a-zA-Z0-9~!@#$%\^&*()_+`\-={}|[\]\:";'<>?,.\/]{6,}$/)).required() //must contain at least 6 char of alphanumeric and all symbol that standart keyboard have
        });
        return errorMessage(schema, req, res, next)
    },

    //middleware for update admin
    adminUpdate : (req,res,next) => {
        const schema = joi.object({
            username : joi.string().allow('').alphanum().min(5).max(30), //allow to empty string but if there is value must contain 5-30 alphanumeric
            pass : joi.string().pattern(new RegExp(/^[a-zA-Z0-9~!@#$%\^&*()_+`\-={}|[\]\:";'<>?,.\/]{6,}$/)).required() //must contain at least 6 char of alphanumeric and all symbol that standart keyboard have
        });
        return errorMessage(schema, req, res, next)
    },

    //function for create article
    articleCreate : (req,res,next) => {
        const schema = joi.object({
            title: joi.string().min(5).max(30).required(), //must contain 5-30 string
            thumbnail: joi.required(), //must include it
            author: joi.string().min(3).max(30).required(), //must contain 3-30 string
            short: joi.string().min(5).required(), //must contain min 5 string
            content: joi.required() //must include it
        })
        return errorMessage(schema, req, res, next)
    },

    //create for update article
    articleUpdate : (req,res,next) => {
        const schema = joi.object({
            title: joi.string().min(5).max(30).allow(''), //allowed empty string but if there is value must contain 5-30 string
            author: joi.string().min(3).max(30).allow(''), //allowed empty string but if there is value must contain 3-30 string
            short: joi.string().min(5).allow(''), //allowed empty string but if there is value at least contain 5 string
        })
        return errorMessage(schema, req, res, next)
    },

    //function for update article
    profileUpdate : (req,res,next) => {
        const schema = joi.object({
            name: joi.string().allow('').min(5).max(30), //allowed empty string but if there is value must contain 5-30 string
            address: joi.string().allow('').min(5), //allowed empty string but if there is value at least contain 5 string
            desc: joi.string().allow('').min(5), //allowed empty string but if there is value at least contain 5 string
            email: joi.string().allow('').email({tlds : false}), //allowed empty string but if there is value must validate as email format
            noHp: joi.string().allow('').pattern(new RegExp('^[+](?:[0-9] ?)[0-9]{6,14}$')), //allowed empty string but if there is value must contain phone number where it start with country code (+)
            ig: joi.string().allow('').min(5).max(30), //allowed empty string but if there is value must contain 5-30 string
            twitter: joi.string().allow('').min(5).max(30), //allowed empty string but if there is value must contain 5-30 string
            fb: joi.string().allow('').min(5).max(30), //allowed empty string but if there is value must contain 5-30 string
            picture: joi.string().allow('') //allowed empty string but if there is value must string format
        })
        return errorMessage(schema, req, res, next)
    },

    //function to create gallery
    galleryCreate : (req,res,next) => {
        const schema = joi.object({
            img: joi.required(), //must incule it
            desc: joi.string().min(5).required(), // value at least contain 5 string
            loc: joi.string().min(3).max(30).required() //value must contain 3-30 string
        })
        return errorMessage(schema, req, res, next)
    },

    //funtion to update gallery
    galleryUpdate : (req,res,next) => {
        const schema = joi.object({
            img: joi.allow(''), //allowed empty string 
            desc: joi.string().allow('').min(5), //allowed empty string but if there is value at least contain 5 string
            loc: joi.string().min(3).allow('').max(30) //allowed empty string but if there is value must contain 3-30 string
        })
        return errorMessage(schema,req,res,next)
    },

    //function to create services
    servicesCreate : (req,res,next) => {
        // if id query included, then validate to create a package
        if (req.query.id) {
            const schema = joi.object({
                thumbnail : joi.required(), //must include it
                package : joi.string().min(3).max(20).required(), //value must contain 3-20 string
                price : joi.number().required(), //must include it
                details : joi.string().min(5).required() //value at least contain 5 string
            })
            return errorMessage(schema,req,res,next)
        }

        //if there is no id wuery included, then validate to create type
        const schema = joi.object({
            type : joi.string().min(3).max(20).required(), //value must contain 3-20 string
            desc : joi.string().min(5).required(), //value at least contain 5 string
            thumbnail : joi.required() //must include it
        })

        return errorMessage(schema,req,res,next)
    },

    //function to update services
    serviceUpdate : (req,res,next) => {
        //if there is id and pack query then validate to update package
        if(req.query.id && req.query.pack){
            const schema = joi.object({
                package : joi.string().min(3).max(20).allow(''), //allowed empty string but if there is value must contain 3-20 string
                price : joi.number().allow(''), //allowed empty string 
                details : joi.string().min(5).allow('') //allowed empty string but if there is value at least contain 5 string
            })
            return errorMessage(schema,req,res,next)
        }

        //if only have id query inside it then validate to update type 
        const schema = joi.object({
            type : joi.string().min(3).max(20).allow(''), //allowed empty string but if there is value must contain 3-20 string
            desc : joi.string().min(5).allow(''), //allowed empty string but if there is value at lesat contain 5 string
            thumbnail : joi.allow('') //allowed empty string 
        })

        return errorMessage(schema,req,res,next)
    }
}