/**
 * checking token is it stored in ./data/token.json 
 * if there is no token where inside the request, then it will return 401
 * if there is no token registered in ./data/token.json, then it will return 401
 * if the token were not suitable with our credential, then it will return wrong credential (401)
 * and if token is request has token inside it, registered in ./data/token.json/, and the credential is suitable
 * then next()
 */

// import all needed dependencies
const fs = require('fs');
const jwt = require('jsonwebtoken');
const file = fs.readFileSync('./data/token.json', 'utf-8');
const data = JSON.parse(file);

// start function
module.exports = function (req,res,next) {

    const auth = req.headers['authorization']; // Request must have token inside it
    const token = auth && auth.split(' ')[1]; // Parsing the token

    //if there is no token inside it
    if (token == null) {
        res.status (401);
        return res.send ({
            message : "token needed"
        })
    }


    // if there is token, then check it to ./data/token.json
    const compare = data.find(x => x == token)
    if (compare) { //if data founded
        // verify the token
        jwt.verify(token, 'bugsBunny', (err)=>{
            if (err) {
                res.send({
                    message : "wrong credential"
                })
                return res.status(401)
            }
        })
        // if the token verified
        return next();
    } 
    
    // if there is token but not registered in ./data/token.json
    res.status(401)
    return res.send({
        message : "token is wrong"
    })
}