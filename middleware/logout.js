/**
 * middleware to deleting the token
 */

// import all needed dependencies
const fs = require('fs')
const tokenFile = fs.readFileSync('./data/token.json', 'utf-8');
const tokens = JSON.parse(tokenFile);
const write = () => fs.writeFileSync('./data/token.json', JSON.stringify(tokens), 'utf-8');

//start function
module.exports = async function (req,res,next) {
    
    const auth = req.headers['authorization']; // the request must have token inside it
    const token = auth && auth.split(' ')[1]; // pasrsing the token

    const idx = tokens.findIndex(x => x == token) // find the token
 
    if (idx > -1) { // if token found
        tokens.splice(idx,1) // delete the token
        write();
        res.send({
            message: "Logout success"
        })
    }
    next();
}