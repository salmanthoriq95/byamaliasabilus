/**
 * when an user has registered as an admin, then they can access all method.
 * in a way that they get the token after they log in.
 * but if they not registered as an admin, but try to access some method it will return "needed token"
 * and if they registered but have wrong password then it will return "wrong password"
 * if the username and password accepted, then it will return the token. and they are become an admin
 */

//import all needed dependencies
const fs = require('fs');
const jwt = require('jsonwebtoken');
const file = fs.readFileSync('./data/users.json', 'utf-8');
const data = JSON.parse(file);
const tokenFile = fs.readFileSync('./data/token.json', 'utf-8');
const tokens = JSON.parse(tokenFile);
const write = () => fs.writeFileSync('./data/token.json', JSON.stringify(tokens), 'utf-8');
const bcrypt = require('bcrypt'); 


// start function
module.exports = async function (req,res,next) {
    // compare the username and password to ./data/users.json
    if (req.body.username && req.body.pass){
        const userData = data.find(x => x.username == req.body.username) // search username
        
        // if fata founded
        if (userData) {
            const compare = await bcrypt.compare(req.body.pass, userData.pass) //compare the password
            if (!compare){ //if password is wrong
                res.status(401)
                return res.send({
                    message : "wrong password"
                })
            }
            //if the password is right then return the token
            const token = jwt.sign({username:userData.username, role: userData.role, login: new Date()}, "bugsBunny");
            tokens.push(token);
            write();
            res.status(200);
            res.send({
                token : token
            })
            next()
        }

    }

    next();
}