# Getting Started

This project was created by nodeJs and React, with NPM dependencies. This is a public API that you can GET article Data that include Article, Profile (The owner of this API), Photos, and Sevices that offered by the Owner.

# Try to Run in Your Local Network

Simply if you want to run in your local network, just run :
### `node app`
or if you installed nodemon :
### `nodemon app`
it would run in your localhost in http://localhost:3001.
if there is no error, when you try to GET http://localhost:3001 it will return :
### `connection OK`
and you can get the data from here


# Parameters to GET Data(s)

# Article
If you want to get all of articles, you simply use GET method to :
### `http://localhost:3001/article`
and it will show all article that API can fetch. 
But if you want to get specific data from article, you can add a `id` parameter. example :
### `http://localhost:3001/article?id=4`
that will return a specific article which it have `id=4`

# Gallery
If you want to get all of gallery, you simply use GET method to :
### `http://localhost:3001/gallery`
and it will show all article that API can fetch. 
But if you want to get specific photo, you can add `id` parameter. example :
### `http://locahost:3001/gallery?id=1`
that will return a specific photo which it have `id=1`

# Profile
if you want to get the profile of the owner, you simpy use GET method to :
### `http://localhost:3001/profile`
and it will show the owner of the API

# Services
Services data divide by it types. Each types is have some package that have different prices offered.

If you want to get all data of services that owner offered, you simply use GET method to :
### `http://localhost:3001/services`
and it will show that services that offered.

if you want get specific type of service data, you can add `id` parameter. example :
### `http://localhost:3001/services?id=1`
it will return "Graduation & Party" services that offered by owner, where it have an `id` valued by 1.


# Error Message
if you gave wrong value of parameters or out of range of parameters, it will return return `null` data with error message
