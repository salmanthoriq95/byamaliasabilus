/*
* this is the first page that will run when you run "node app" or "node JS".
* run in your localhost at port 3001
* 
* using express version 4.17.1 for backend dependencies,
* using jsonwebtoken(jwt) version 8.5.1 for token dependencies,
* using joi version 13.6.0 for validator dependencies,
* using cors version 2.8.5 for cors dependencies
* using bcrypt version version 5.0.1 for encrypt dependencies
* and you can look all dependencies at package.json
* 
* the flow of this page is :
* 1. import all needed dependencies
* 2. using library from express to parsing json and form-urlencoded to all path
* 3. using cors to resolve cors origin policy
* 4. using middleware to know who accessed the page
* 5. use router where it stored in ./router/index.js file to routing the page
* 6. return a notFound Page when user inputing wrong route
* 7. after all deeclaired then up the server in port 3001
* 
* for login, default login is :
* username : admin
* pass : admin
*/

//server init
const express = require('express'); //import express module
const app = express(); //using express module
const port = 3001; //declaire port
const cors = require('cors'); //cors policy setting
const router = require('./router'); //import router
const jwt = require('jsonwebtoken'); 
const mware = require('./middleware');

app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

//cors origin
app.use(cors());

//middleware
app.use(mware.login) //if user want to login

//call router
app.use(router)

//middleware when page not found
app.use(mware.notFound)

//start server
app.listen(port, () => {
  console.log(`listening at port : ${port}`)
});