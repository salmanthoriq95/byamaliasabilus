/*
* this page will accessed when you only declaired the page in other page.
* make it easy when you want to destructuring function in ES 6
* 
* for example, when you want to access admin only you can declaire :
* const {admin} = require ('./controller')
* 
* or you can access whole middleware and call it like an object. example :
* const controller = require ('./controller')
* controller.admin
* to check the token
*/



module.exports = {
	home : require('./home'),
    article : require('./article'),
    profile : require('./profile'),
    gallery : require('./gallery'),
    services : require('./services'),
    admin : require('./admin'),
}