/**
 * route to /article
 * user only can use GET method
 * admin can use all method, and manipulate the datas
 */

//import all dependencies
const fs = require('fs'); //use file system
const file = fs.readFileSync('./data/article.json', 'utf-8'); //read article.json
const rawData = JSON.parse(file); //parsing file to object
const data = rawData.article; //get the article from data
const write = () => fs.writeFileSync('./data/article.json', JSON.stringify(rawData), 'utf-8'); //re-write data to article.json
const date = new Date(); //get realtime date
const month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"] //identify month from the Date()
/**
 * create object constructor for message
 * 
 * @param {number} status return status code to front. true if ok, false if not ok 
 * @param {string} message return a message when it error or ok
 * @param {object} data return data. if error then data is null
 * @param {*} method return method that user requested
 * @param {*} url return url that user accessed
 */
function msg (status, message, data, method, url) {
    this.status = status;
    this.message = message;
    this.data = data;
    this.method = method;
    this.url = url;
}

module.exports = {
    //show all data
    index : (req,res)=>{
        //if the id query is included with the request
        if (req.query.id){
            //find the article by comparing it id
            const find = data.find(x => x.id == req.query.id);
            if (find) {
                //when found
                res.status(200);
                return res.send(new msg(
                    true, 
                    'Success to show data', 
                    find, 
                    req.method, 
                    req.url
                ));
            } else {
                //when not found
                res.status(404);
                return res.send(new msg(
                    false, 
                    'failed to show data, wrong id!', 
                    null, 
                    req.method, 
                    req.url
                ));
            }
        }

        //if there is no data or article is empty
        if (data.length<=0){
            res.status(404);
			return res.send(new msg(
                false, 
                'failed to show data, there is no data!', 
                null, 
                req.method, 
                req.url
            ));

        };

        //showing all articles
        res.status(200);
        res.send(new msg(
            true, 
            'Success to show all datas', 
            data, 
            req.method, 
            req.url
        ));
    },

    //create new data
    create : (req,res)=>{

        //create new id
        let id = data.map(x => x.id);
        id = Math.max.apply(null,id);

        //create new data
        const newData = {
            id : id+1,
            title : req.body.title,
            thumbnail : req.body.thumbnail,
            author : req.body.author,
            short : req.body.short,
            content : req.body.content,
            date : {
                day : date.getDate(),
                month : month[date.getMonth()],
                year : date.getFullYear()
            }
        }

        //place new data in front of array
        data.unshift(newData)
        write(); //re-write the data

        res.status(200);
        res.send(new msg(
            true, 
            'Success to create new data', 
            data, 
            req.method, 
            req.url
        ));
    },

    //update a data
    update : (req,res)=>{
        //find the data by it id
        const edit = data.filter(x => {
            //if the data found then change it value. if there is empty string among the new data then fill with old data
            if(x.id == req.query.id){
                x.title = req.body.title? req.body.title : x.title;
                x.thumbnail = req.body.thumbnail? req.body.thumbnail : x.thumbnail,
                x.content = req.body.content? req.body.content : x.content;
                x.author = req.body.author? req.body.author: x.author;
                x.short = req.body.short? req.body.short : x.short,
                x.date.day = date.getDate();
                x.date.month = month[date.getMonth()];
                x.date.year = date.getFullYear();
                return x;
            }
        });

        //if data not found
        if (edit.length<=0){
			res.status(404);
			return res.send(new msg(
                false, 
                'Data not found!', 
                null, 
                req.method, 
                req.url
            ));
		}
        
        //re-write data
        write();
        res.status(200);
        res.send(new msg(
            true, 
            'Success to edit data', 
            data, 
            req.method, 
            req.url
        ));
    },

    //delete a data
    delete : (req, res)=>{
        //find the data by it index, with id as paramete. so we can easy to splice it
        const idx = data.findIndex(x => x.id == req.query.id);

        //if data is not found
        if (idx < 0){
            res.status(404);
			return res.send(new msg(
                false, 
                'Data not found!', 
                null, 
                req.method, 
                req.url
            ));
        };

        //if data is exist then splice it
        data.splice(idx,1);
		res.status(200);
		res.send(new msg(
            true, 
            'Success deleted a data', 
            data, 
            req.method, 
            req.url
        ))
		write(); //re-write the data

    },

}