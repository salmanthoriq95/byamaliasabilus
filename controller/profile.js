/**
 * route to /profile
 * only have 2 methods, that GET and PUT
 * user only can use GET method
 * admin can use all method, and manipulate the datas
 */

//import all dependencies
const fs = require('fs'); //import file system
const file = fs.readFileSync('./data/profile.json', 'utf-8'); //read article.json
const rawData = JSON.parse(file); //parsing the data become an object
const data = rawData.profile; //get the profile from the rawData
const write = () => fs.writeFileSync('./data/profile.json', JSON.stringify(rawData), 'utf-8'); //re-write the data
/**
 * create object constructor for message
 * 
 * @param {number} status return status code to front. true if ok, false if not ok 
 * @param {string} message return a message when it error or ok
 * @param {object} data return data. if error then data is null
 * @param {*} method return method that user requested
 * @param {*} url return url that user accessed
 */
function msg (status, message, data, method, url) {
    this.status = status;
    this.message = message;
    this.data = data;
    this.method = method;
    this.url = url;
}

module.exports = {
    //show all data
    index : (req,res)=>{
        res.status(200);
        res.send(new msg(
            true, 
            'Success to show profie', 
            data, 
            req.method, 
            req.url
        ));
    },

    //update a data
    update : (req,res)=>{
        console.log(req.body)
        const edit = data.filter(x => {
            if(x.id == req.params.id){
                x.name = req.body.name? req.body.name : x.name;
                x.address = req.body.address? req.body.address : x.address; 
                x.desc = req.body.desc? req.body.desc : x.desc ;
                x.contact.email = req.body.email? req.body.email : x.contact.email;
                x.contact.noHp = req.body.noHp? req.body.noHp : x.contact.noHp;
                x.socmed.ig = req.body.ig? req.body.ig : x.socmed.ig;
                x.socmed.twitter = req.body.twitter? req.body.twitter : x.socmed.twitter;
                x.socmed.fb = req.body.fb? req.body.fb : x.socmed.fb;
                x.picture = req.body.picture? req.body.picture : x.picture;
                return x;
            }
        });

        write();
        res.status(200);
        res.send(new msg(
            true, 
            'Success to updating profile', 
            data, 
            req.method, 
            req.url
        ));
    }
}