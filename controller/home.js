/**
 * route to /
 * all can accessing this page
 * to confirm that the api is up
 */
module.exports = {
	//GET home
	index : (req, res) => {
		res.send('connection OK');
	}
}