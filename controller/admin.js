/**
 * route to /admin
 * where only admin can access all methods,
 * encrypt data with bcrypt
 */
//import all dependencies
const fs = require('fs'); //to access file system
const file = fs.readFileSync('./data/users.json', 'utf-8'); //acessing users.json
const data = JSON.parse(file); //parsing data to be an object
const write = () => fs.writeFileSync('./data/users.json', JSON.stringify(data), 'utf-8'); //re-write data to users.json
const bcrypt = require('bcrypt'); //encrypt data
/**
 * create object constructor for message
 * 
 * @param {number} status return status code to front. true if ok, false if not ok 
 * @param {string} message return a message when it error or ok
 * @param {object} data return data. if error then data is null
 * @param {*} method return method that user requested
 * @param {*} url return url that user accessed
 */
function msg (status, message, data, method, url) { 
    this.status = status;
    this.message = message;
    this.data = data;
    this.method = method;
    this.url = url;
}

//start module
module.exports = {
    //show all data
    index : (req,res)=>{
        res.status(200);
        return res.send(new msg(
            true, 
            'Success to show data', 
            data, 
            req.method, 
            req.url
        ));
    },

    //create new data
    create : async (req,res)=>{
        //find username
        const findData = data.find(x => x.username === req.body.username);
        //if username founded then return it has been registered
        if (findData) {
            res.status(411);
            return res.send(new msg(
                false, 
                'Username already exist', 
                null, 
                req.method, 
                req.url
            ));
        }

        //create id
        let id = data.map(x => x.id);
        id = Math.max.apply(null,id);

        //create new user object
        const newData = {
            id : id+1,
            username : req.body.username,
            pass : await bcrypt.hash(req.body.pass,12),
            role : req.body.role
        }
        data.push(newData) //push data
        write(); //rewrite data

        res.status(200);
        return res.send(new msg(
            true, 
            'Success to create new data', 
            data, 
            req.method, 
            req.url
        ));

    },

    //update a data
    update : async (req,res)=>{

        //encrypt password
        const password = req.body.pass? await bcrypt.hash(req.body.pass,12) : null;
        
        //find data user with it index, so we can identify when other username is exist
        const edit = data.filter((x,i) => {
            
            //if founded
            if (x.id == req.query.id){
                
                /**
                 * checking the username
                 * if the username is exist and it not based by it index
                 * then return false
                 * but, if the name is exist and based by it index
                 * then next
                 */
                const usern = data.find((data, idx) => i!=idx && req.body.username == data.username)                
                if (usern) {
                    res.status(411);
                    return res.send(new msg(
                        false, 
                        'username already exist', 
                        null, 
                        req.method, 
                        req.url
                    ));
                }

                /**
                 * if username is not registered
                 * 1. enter new username if there is value or use old username if username is empty
                 * 2. assign new encrypt pass
                 * 3. assign the role
                 */
                x.username = req.body.username? req.body.username : x.username;
                x.pass = req.body.pass? password : x.pass;
                x.role = req.body.role? req.body.role : x.role;

                write(); //rewrite data
                res.status(200);
                return res.send(new msg(
                    true, 
                    'Success to edit data', 
                    data, 
                    req.method, 
                    req.url
                ));
                
                return x;
            }
        });
        
        //if data not found
        if (edit.length<=0){
			res.status(404);
			return res.send(new msg(
                false, 
                'Data not found!', 
                null, 
                req.method, 
                req.url
            ));
		}

        

    },

    //delete a data
    delete : (req, res)=>{
        //find it index, by form body. so we can splice it with it index
        const idx = data.findIndex(x => x.id == req.query.id);
        //if not found
        if (idx < 0){
            res.status(404);
			return res.send(new msg(
                false, 
                'Data not found!', 
                null, 
                req.method, 
                req.url
            ));
        };

        //when data found, then splice it from array
        data.splice(idx,1);
        write(); //re-write data
		res.status(200);
		return res.send(new msg(
            true, 
            'Success deleted a data', 
            data, 
            req.method, 
            req.url
        ))
    },
}