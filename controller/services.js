/**
 * route to /services
 * user only can use GET method
 * admin can use all method, and manipulate the datas
 */

//import all dependencies
const fs = require('fs'); //import file system
const file = fs.readFileSync('./data/services.json', 'utf-8'); //read the services.json
const rawData = JSON.parse(file); //parsing data become an object
const data = rawData.services; //get the service data from rawData
const write = () => fs.writeFileSync('./data/services.json', JSON.stringify(rawData), 'utf-8'); //re-write data
/**
 * create object constructor for message
 * 
 * @param {number} status return status code to front. true if ok, false if not ok 
 * @param {string} message return a message when it error or ok
 * @param {object} data return data. if error then data is null
 * @param {*} method return method that user requested
 * @param {*} url return url that user accessed
 */
function msg (status, message, data, method, url) {
    this.status = status;
    this.message = message;
    this.data = data;
    this.method = method;
    this.url = url;
}

module.exports = {
    //show all data
    index : (req,res)=>{
        //checking query input
        if (req.query.id){ //if there is id from the query
            //find the id
            const find = data.find(x => x.id == req.query.id);
            if (find) {
                //if found then show specific data
                res.status(200);
                return res.send(new msg(
                    true, 
                    'Success to show data', 
                    find, 
                    req.method, 
                    req.url
                ));
            } else {
                //if not found then send error
                res.status(404);
                return res.send(new msg(
                    false, 
                    'failed to show data, wrong id!', 
                    null, 
                    req.method, 
                    req.url
                ));
            }
        }

        //if there is no data
        if (data.length<=0){
            res.status(404);
			return res.send(new msg(
                false, 
                'failed to show data, there is no data!', 
                null, 
                req.method, 
                req.url
            ));

        };

        //if request without the id query
        res.status(200);
        res.send(new msg(
            true, 
            'Success to show all datas', 
            data, 
            req.method, 
            req.url
        ));
    },

    //create new data
    create : (req,res)=>{
        //if the request give an id query
        if (req.query.id){
            //find the id query form "type" data
            const findQ = data.find(x => x.id == req.query.id);
            //if the id not found then send error
            if (!findQ){
                res.status(404);
                return res.send(new msg(
                    false, 
                    'failed to show data, there is no data!', 
                    null, 
                    req.method, 
                    req.url
                ));
            }

            //looping the length of "type" data of array and return to id. to get the max length of array
            let id = findQ.prices.map(x => x.id);
            //ifthe length <0 then give id = 0, else get the max length
            id.length <= 0 ? id = 0 : id = Math.max.apply(null,id)
            //create new package, will store in price array
            const newPackage = {
                id : Number(id)+1,
                thumbnail: req.body.thumbnail,
                package: req.body.package,
                price: req.body.price,
                details: req.body.details
            }
            //push the data to "price" array
            findQ.prices.push(newPackage);
            write(); //re-write the data
            //send status
            res.status(200);
            return res.send(new msg(
                true, 
                'Success to add new package', 
                findQ, 
                req.method, 
                req.url
            ));
        } 
        
        //create new id for "type"
        let id = data.map(x => x.id);
        id = Math.max.apply(null,id);

        //create new data for "type"
        const newData = {
            id : id+1,
            type : req.body.type,
            desc : req.body.desc,
            thumbnail : req.body.thumbnail,
            prices: []
        }

        //push data
        data.push(newData)
        write(); //re-write

        //send status data
        res.status(200);
        res.send(new msg(
            true, 
            'Success to create new data', 
            data, 
            req.method, 
            req.url
        ));
    },

    //update a data
    update : (req,res)=>{
        //find the specific data by compiring it id
        const edit = data.find(x => x.id == req.query.id)

        //if the query is valued by it id and package, then edit the specific "price" in array
        if(req.query.id && req.query.pack){
            //find the specific package in "price array"
            const editPack = edit.prices.find(x => x.id == req.query.pack)
            //if found then edit the value. if it gave the empty string then fill it with old value
            if (editPack) {
                editPack.thumbnail = req.body.thumbnail? req.body.thumbnail : editPack.thumbnail;
                editPack.package = req.body.package? req.body.package : editPack.package;
                editPack.price = req.body.price? req.body.price : editPack.price;
                editPack.details = req.body.details? req.body.details : editPack.details;
                
                write(); //re-write data

                res.status(200);
                return res.send(new msg(
                    true, 
                    'Success to edit package', 
                    data, 
                    req.method, 
                    req.url
                ));
            } else {
                //if package data not found by it "pack" query, then send error
                res.status(404);
                return res.send(new msg(
                    false, 
                    'data not found!', 
                    null, 
                    req.method, 
                    req.url
                ));
            }
        }
        
        //if query only give an id then edit the "type"
        if(edit){
            //if it gave empty string then it will fill in with old value
            edit.type = req.body.type? req.body.type : edit.type;
            edit.desc = req.body.desc? req.body.desc : edit.desc;
            edit.thumbnail = req.body.thumbnail? req.body.thumbnail : edit.thumbnail;
            write(); //re-write dta
            res.status(200);
            return res.send(new msg(
                true, 
                'Success to edit package', 
                data, 
                req.method, 
                req.url
            ));
        }

            //if data is not found
			res.status(404);
			return res.send(new msg(
                false, 
                'Data not found!', 
                null, 
                req.method, 
                req.url
            ));
    },

    //delete a data
    delete : (req, res)=>{

        //if the id and pack query is exist, then it mean to delete specific package in "price"
        if(req.query.id && req.query.pack){
            //find the type first, then get the index of package in "price"
            const edit = data.find(x => x.id == req.query.id)
            const idx = edit.prices.findIndex(x => x.id == req.query.pack)
           
            //if founded then delete package from the "price"
            if (idx>-1) {
                edit.prices.splice(idx,1);
                write();
                res.send(new msg(
                    true, 
                    'Success deleted a data', 
                    data, 
                    req.method, 
                    req.url
                ))
                return res.status(200);
            } else {
                //if not found send error
                res.status(404);
                return res.send(new msg(
                    false, 
                    'data not found!', 
                    null, 
                    req.method, 
                    req.url
                ));
            }
        }

        //if the query is only an id, that mean to delete the type and whole package
        //then get the index, so we can easy to delete it from array
        const idx = data.findIndex(x => x.id == req.query.id);
        
        //if the id is not found then send error
        if (idx < 0){
            res.status(404);
			return res.send(new msg(
                false, 
                'Data not found!', 
                null, 
                req.method, 
                req.url
            ));
        };

        //delete the data by it id
        data.splice(idx,1);
        write(); //re-write data
        //send status
		res.status(200);
		res.send(new msg(
            true, 
            'Success deleted a data', 
            data, 
            req.method, 
            req.url
        ))

    },
}