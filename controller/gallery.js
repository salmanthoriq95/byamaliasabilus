/**
 * route to /gallery
 * user only can use GET method
 * admin can use all method, and manipulate the datas
 */

//import all dependencies
const fs = require('fs'); //import file system
const file = fs.readFileSync('./data/gallery.json', 'utf-8'); //read gallery.json
const rawData = JSON.parse(file); //parse the data become an object
const data = rawData.gallery; //get the gallery data from rawData
const write = () => fs.writeFileSync('./data/gallery.json', JSON.stringify(rawData), 'utf-8'); //re-write data
const date = new Date(); //get the realtime date
const month = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"] //identify the month from the Date()
/**
 * create object constructor for message
 * 
 * @param {number} status return status code to front. true if ok, false if not ok 
 * @param {string} message return a message when it error or ok
 * @param {object} data return data. if error then data is null
 * @param {*} method return method that user requested
 * @param {*} url return url that user accessed
 */
function msg (status, message, data, method, url) {
    this.status = status;
    this.message = message;
    this.data = data;
    this.method = method;
    this.url = url;
}

module.exports = {
    //show all data
    index : (req,res)=>{
        //if id query is included with the request
        if (req.query.id){
            //find the data
            const find = data.find(x => x.id == req.query.id);
            if (find) {
                //if the data is found, show specific data
                res.status(200);
                return res.send(new msg(
                    true, 
                    'Success to show data', 
                    find, 
                    req.method, 
                    req.url
                ));
            } else {
                //if the data is not found
                res.status(404);
                return res.send(new msg(
                    false, 
                    'failed to show data, wrong id!', 
                    null, 
                    req.method, 
                    req.url
                ));
            }
        }

        //if there is no data inside the gallery
        if (data.length<=0){
            res.status(404);
			return res.send(new msg(
                false, 
                'failed to show data, there is no data!', 
                null, 
                req.method, 
                req.url
            ));

        };

        
        //if there is data, show all datas
        res.status(200);
        res.send(new msg(
            true, 
            'Success to show all datas', 
            data, 
            req.method, 
            req.url
        ));
    },

    //create new data
    create : (req,res)=>{

        //create new id
        let id = data.map(x => x.id);
        id = Math.max.apply(null,id);

        //create new data
        const newData = {
            id : id+1,
            img : req.body.img,
            desc : req.body.desc,
            loc : req.body.loc,
            date : {
                day : date.getDate(),
                month : month[date.getMonth()],
                year : date.getFullYear()
            }
        }

        //put data in front of array
        data.unshift(newData)
        write(); //re-write data

        //send status
        res.status(200);
        res.send(new msg(
            true, 
            'Success to create new data', 
            data, 
            req.method, 
            req.url
        ));
    },

    //update a data
    update : (req,res)=>{

        //searching data by it id
        const edit = data.filter(x => {
            //if found then edit the data
            if(x.id == req.query.id){
                x.img = req.body.img? req.body.img : x.img;
                x.desc = req.body.desc? req.body.desc : x.desc;
                x.loc = req.body.loc? req.body.loc : x.loc;
                return x;
            }
        });

        //if data is not found, send error
        if (edit.length<=0){
			res.status(404);
			return res.send(new msg(
                false, 
                'Data not found!', 
                null, 
                req.method, 
                req.url
            ));
		}
        
        //rewrite data
        write();
        //send status
        res.status(200);
        res.send(new msg(
            true, 
            'Success to edit data', 
            data, 
            req.method, 
            req.url
        ));
    },

    //delete a data
    delete : (req, res)=>{

        //search data by it index, so we can easy to splice it
        const idx = data.findIndex(x => x.id == req.query.id);

        //if data is not found, send error
        if (idx < 0){
            res.status(404);
			return res.send(new msg(
                false, 
                'Data not found!', 
                null, 
                req.method, 
                req.url
            ));
        };

        //delete (splice) data
        data.splice(idx,1);
        //send status
		res.status(200);
		res.send(new msg(
            true, 
            'Success deleted a data', 
            data, 
            req.method, 
            req.url
        ))
		write();

    },
}