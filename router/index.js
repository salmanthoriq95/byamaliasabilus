/*
* this is the route of all methods of all page
* some spcefic methods not allowed to user, only admin can access it.
* admin can confirmed by it token where it will checked in middleware.check funtion where stored in ./middleware file
* and only GET method that can be accessed without token.
* if user try to access the page without token and try to fetch method besides GET, it will return message "token needed.
*/

//import express library and init express router
const express = require('express');
const router = express.Router();

/*
* import controller and middleware function
* token will confirmed with middleware.check
* "validator" will validate the input of user before it acessed to funtion 
* all function of middleware is declaired in ./middleware
* all function to manipulate data is declaired in ./controller
*/
const controller = require('../controller');
const middleware = require('../middleware');
const {validator} = require('../middleware')


//HTTP REQUEST
//home
router.route('/')
	.get(controller.home.index)
    .delete(middleware.logout)

//article
router.route('/article')
    .get(controller.article.index)
    .post(middleware.check, validator.articleCreate, controller.article.create)
    .put(middleware.check, validator.articleUpdate, controller.article.update)
    .delete(middleware.check, controller.article.delete);


//profile
router.route('/profile')
    .get(controller.profile.index)
    .put(middleware.check, validator.profileUpdate,controller.profile.update);

//gallery
router.route('/gallery')
    .get(controller.gallery.index)
    .post(middleware.check, validator.galleryCreate, controller.gallery.create)
    .put(middleware.check, validator.galleryUpdate, controller.gallery.update)
    .delete(middleware.check, controller.gallery.delete);

//services
router.route('/services')
    .get(controller.services.index)
    .post(middleware.check, validator.servicesCreate, controller.services.create)
    .delete(middleware.check,  controller.services.delete)
    .put(middleware.check, validator.serviceUpdate, controller.services.update);

//admin
router.route('/admin')
    .get(middleware.check, controller.admin.index)
    .post(middleware.check, validator.adminCreate, controller.admin.create)
    .delete(middleware.check, controller.admin.delete)
    .put(middleware.check, validator.adminUpdate, controller.admin.update)

module.exports = router